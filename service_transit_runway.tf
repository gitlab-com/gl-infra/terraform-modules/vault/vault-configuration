// Transit secrets engine for Runway
resource "vault_mount" "transit-runway" {
  path        = local.runway_transit_mount_path
  type        = "transit"
  description = "Transit secrets engine for Runway"
}
