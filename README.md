# GitLab.com Vault Configuration Terraform Module

## What is this?

This module provisions the configuration of a Vault instance.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | >= 3.14.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_vault"></a> [vault](#provider\_vault) | >= 3.14.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [vault_approle_auth_backend_role.provisioning](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/approle_auth_backend_role) | resource |
| [vault_approle_auth_backend_role.role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/approle_auth_backend_role) | resource |
| [vault_audit.file](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/audit) | resource |
| [vault_auth_backend.approle](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/auth_backend) | resource |
| [vault_auth_backend.gcp](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/auth_backend) | resource |
| [vault_auth_backend.kubernetes](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/auth_backend) | resource |
| [vault_gcp_auth_backend_role.gce-chef](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/gcp_auth_backend_role) | resource |
| [vault_gcp_auth_backend_role.raft-snapshots](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/gcp_auth_backend_role) | resource |
| [vault_gcp_secret_backend.gcp](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/gcp_secret_backend) | resource |
| [vault_gcp_secret_impersonated_account.account](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/gcp_secret_impersonated_account) | resource |
| [vault_gcp_secret_roleset.roleset](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/gcp_secret_roleset) | resource |
| [vault_gcp_secret_static_account.account](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/gcp_secret_static_account) | resource |
| [vault_identity_group.okta_group](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/identity_group) | resource |
| [vault_identity_group_alias.okta_group](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/identity_group_alias) | resource |
| [vault_identity_group_policies.policies](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/identity_group_policies) | resource |
| [vault_jwt_auth_backend.jwt](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend) | resource |
| [vault_jwt_auth_backend.okta](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend) | resource |
| [vault_jwt_auth_backend_role.admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend_role) | resource |
| [vault_jwt_auth_backend_role.user](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/jwt_auth_backend_role) | resource |
| [vault_kubernetes_auth_backend_config.cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_auth_backend_config) | resource |
| [vault_kubernetes_auth_backend_role.identity-entities-cleaner](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_auth_backend_role) | resource |
| [vault_kubernetes_auth_backend_role.role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_auth_backend_role) | resource |
| [vault_kubernetes_secret_backend.cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_secret_backend) | resource |
| [vault_kubernetes_secret_backend_role.role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_secret_backend_role) | resource |
| [vault_kv_secret_v2.chef-cookbook-placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_kv_secret_v2.chef-env-shared-placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_kv_secret_v2.chef-shared-placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_kv_secret_v2.kubernetes_env_ns_placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_kv_secret_v2.kubernetes_env_placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_kv_secret_v2.kubernetes_namespace_placeholder](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |
| [vault_mount.chef](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.ci](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.kubernetes](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.kv](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.runway](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.shared](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.transit-ci](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_mount.transit-runway](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_policy.admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.chef_role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.ci_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.ci_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.ci_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gcp_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gcp_impersonated_account](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gcp_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gcp_roleset](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gcp_static_account](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gitlab-management](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gitlab-management-rw](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gitlab_ci_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gitlab_ci_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.gitlab_ci_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.identity-entities-cleaner](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_readonly_cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.k8s_role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_admin_cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_list_cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.policy](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.raft-snapshots](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway-provisioner](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway-provisioner-rw](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.runway_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.shared_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.shared_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.shared_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.shared_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.shared_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.shared_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.terraform](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.vault-provisioning](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy_document.admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.chef_role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.ci_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.ci_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.ci_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gcp_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gcp_impersonated_account](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gcp_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gcp_roleset](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gcp_static_account](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gitlab-management](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gitlab-management-rw](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gitlab_ci_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gitlab_ci_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.gitlab_ci_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.identity-entities-cleaner](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_readonly_cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.k8s_role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.kubernetes_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.kubernetes_admin_cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.kubernetes_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.kubernetes_list_cluster](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.kubernetes_role](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.raft-snapshots](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.readonly](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway-provisioner](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway-provisioner-rw](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.runway_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.shared_admin_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.shared_identity_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.shared_identity_list](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.shared_identity_read](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.shared_list_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.shared_readonly_all](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.terraform](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |
| [vault_policy_document.vault-provisioning](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/data-sources/policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_admin_groups"></a> [admin\_groups](#input\_admin\_groups) | Okta groups allowed admin access to Vault. | `list(string)` | `[]` | no |
| <a name="input_admin_oidc_logging"></a> [admin\_oidc\_logging](#input\_admin\_oidc\_logging) | Log received OIDC tokens and claims from admin users when debug-level logging is active. | `bool` | `false` | no |
| <a name="input_chef_environments"></a> [chef\_environments](#input\_chef\_environments) | Map of Chef environments with roles. | <pre>map(object({<br/>    gcp_projects = optional(list(string), [])<br/>    roles = map(object({<br/>      cookbooks             = optional(list(string), [])<br/>      gcp_instance_groups   = optional(list(string), [])<br/>      gcp_labels            = optional(list(string), [])<br/>      gcp_zones             = optional(list(string), [])<br/>      policies              = optional(list(string), [])<br/>      readonly_secret_paths = optional(list(string), [])<br/>    }))<br/>  }))</pre> | `{}` | no |
| <a name="input_chef_secrets_policies"></a> [chef\_secrets\_policies](#input\_chef\_secrets\_policies) | Map of Chef K/V secret paths with lists of Okta groups allowed to assume each admin\|read\|list role. | <pre>map(object({<br/>    admin = optional(object({ groups = list(string) }), { groups = [] })<br/>    read  = optional(object({ groups = list(string) }), { groups = [] })<br/>    list  = optional(object({ groups = list(string) }), { groups = [] })<br/>  }))</pre> | `{}` | no |
| <a name="input_ci_secrets_path_max_depth"></a> [ci\_secrets\_path\_max\_depth](#input\_ci\_secrets\_path\_max\_depth) | Maximum depth for CI secret paths, used for created `.placeholder` secrets. | `number` | `10` | no |
| <a name="input_ci_secrets_policies"></a> [ci\_secrets\_policies](#input\_ci\_secrets\_policies) | Map of CI K/V secret paths with lists of Okta groups allowed to assume each admin\|read\|list role. | <pre>map(object({<br/>    admin = optional(object({ groups = list(string) }), { groups = [] })<br/>    read  = optional(object({ groups = list(string) }), { groups = [] })<br/>    list  = optional(object({ groups = list(string) }), { groups = [] })<br/>  }))</pre> | `{}` | no |
| <a name="input_gcp"></a> [gcp](#input\_gcp) | GCP Secrets Engine configuration + static accounts, impersonated accounts and rolesets per project. | <pre>object({<br/>    credentials       = optional(string)<br/>    default_lease_ttl = optional(number, 3600)<br/>    max_lease_ttl     = optional(number, 10800)<br/>    projects = optional(map(object({<br/>      impersonated_accounts = optional(map(object({<br/>        service_account_id = optional(string)<br/>        oauth_scopes       = optional(list(string), ["https://www.googleapis.com/auth/cloud-platform"])<br/>      })), {})<br/>      rolesets = optional(map(object({<br/>        additional_bindings = optional(list(object({<br/>          resource = string<br/>          roles    = list(string)<br/>        })), [])<br/>        oauth_scopes = optional(list(string), ["https://www.googleapis.com/auth/cloud-platform"])<br/>        roles        = list(string)<br/>        type         = optional(string, "access_token")<br/>      })), {})<br/>      static_accounts = optional(map(object({<br/>        additional_bindings = optional(list(object({<br/>          resource = string<br/>          roles    = list(string)<br/>        })), [])<br/>        service_account_id = optional(string)<br/>        type               = optional(string, "access_token")<br/>        oauth_scopes       = optional(list(string), ["https://www.googleapis.com/auth/cloud-platform"])<br/>      })), {})<br/>    })), {})<br/>  })</pre> | `{}` | no |
| <a name="input_gcp_policies"></a> [gcp\_policies](#input\_gcp\_policies) | Map of GCP projects and rolesets / service accounts with lists of Okta groups allowed to generate access tokens. | <pre>map(object({<br/>    impersonated_accounts = optional(map(object({ groups = optional(list(string), []) })), {})<br/>    rolesets              = optional(map(object({ groups = optional(list(string), []) })), {})<br/>    static_accounts       = optional(map(object({ groups = optional(list(string), []) })), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_identity_entities_cleaner_role"></a> [identity\_entities\_cleaner\_role](#input\_identity\_entities\_cleaner\_role) | Kubernetes role used for cleaning old identity entities. | <pre>object({<br/>    name            = optional(string, "identity-entities-cleaner")<br/>    cluster         = string<br/>    namespace       = optional(string, "vault")<br/>    service_account = optional(string, "vault-identity-entities-cleaner")<br/>    token_max_ttl   = optional(number, 3600)<br/>    token_ttl       = optional(number, 3600)<br/>  })</pre> | n/a | yes |
| <a name="input_jwt_auth_backends"></a> [jwt\_auth\_backends](#input\_jwt\_auth\_backends) | Map of JWT auth backends (for GitLab CI or others). | <pre>map(object({<br/>    description        = string<br/>    oidc_discovery_url = string<br/>    bound_issuer       = string<br/>    default_lease_ttl  = optional(string, "1h")<br/>    max_lease_ttl      = optional(string, "3h")<br/>  }))</pre> | `{}` | no |
| <a name="input_kubernetes_clusters"></a> [kubernetes\_clusters](#input\_kubernetes\_clusters) | Map of Kubernetes clusters with their configuration, authentication roles and secrets roles. | <pre>map(object({<br/>    config = object({<br/>      host                = string<br/>      ca_cert             = string<br/>      service_account_jwt = optional(string)<br/>      token_reviewer_jwt  = optional(string)<br/>    })<br/>    environment = string<br/>    auth_roles = optional(map(object({<br/>      namespaces             = list(string)<br/>      service_accounts       = list(string)<br/>      readonly_secret_paths  = optional(list(string), [])<br/>      readwrite_secret_paths = optional(list(string), [])<br/>      policies               = optional(list(string), [])<br/>    })), {})<br/>    secrets_roles = optional(map(object({<br/>      allowed_kubernetes_namespaces = list(string)<br/>      extra_annotations             = optional(map(string), {})<br/>      extra_labels                  = optional(map(string), {})<br/>      name_template                 = optional(string)<br/>      role_name                     = optional(string)<br/>      role_type                     = optional(string, "Role")<br/>      role_rules = optional(list(object({<br/>        apiGroups       = optional(list(string))<br/>        nonResourceURLs = optional(list(string))<br/>        resourceNames   = optional(list(string))<br/>        resources       = optional(list(string))<br/>        verbs           = optional(list(string))<br/>      })), [])<br/>      service_account_name = optional(string)<br/>      token_default_ttl    = optional(number, 3600)<br/>      token_max_ttl        = optional(number, 10800)<br/>    })), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_kubernetes_policies"></a> [kubernetes\_policies](#input\_kubernetes\_policies) | Map of Kubernetes clusters and roles with lists of Okta groups allowed to generate access tokens. | <pre>map(object({<br/>    roles = optional(map(object({ groups = optional(list(string), []) })), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_kubernetes_secrets_policies"></a> [kubernetes\_secrets\_policies](#input\_kubernetes\_secrets\_policies) | Map of Kubernetes K/V secret paths with lists of Okta groups allowed to assume each admin\|read\|list role. | <pre>map(object({<br/>    admin = optional(object({ groups = list(string) }), { groups = [] })<br/>    read  = optional(object({ groups = list(string) }), { groups = [] })<br/>    list  = optional(object({ groups = list(string) }), { groups = [] })<br/>  }))</pre> | `{}` | no |
| <a name="input_okta_oidc"></a> [okta\_oidc](#input\_okta\_oidc) | Okta OIDC configuration. | <pre>object({<br/>    client_id     = string<br/>    client_secret = string<br/>    discovery_url = string<br/>  })</pre> | n/a | yes |
| <a name="input_raft_snapshots_service_account"></a> [raft\_snapshots\_service\_account](#input\_raft\_snapshots\_service\_account) | Service Account used by the raft-snapshot job to authenticate to Vault. | `string` | n/a | yes |
| <a name="input_runway_secrets_policies"></a> [runway\_secrets\_policies](#input\_runway\_secrets\_policies) | Map of Runway K/V secret paths with lists of Okta groups allowed to assume each admin\|read\|list role. | <pre>map(object({<br/>    admin = optional(object({ groups = list(string) }), { groups = [] })<br/>    read  = optional(object({ groups = list(string) }), { groups = [] })<br/>    list  = optional(object({ groups = list(string) }), { groups = [] })<br/>  }))</pre> | `{}` | no |
| <a name="input_shared_secrets_policies"></a> [shared\_secrets\_policies](#input\_shared\_secrets\_policies) | Map of shared K/V secret paths with lists of Okta groups allowed to assume each admin\|read\|list role. | <pre>map(object({<br/>    admin = optional(object({ groups = list(string) }), { groups = [] })<br/>    read  = optional(object({ groups = list(string) }), { groups = [] })<br/>    list  = optional(object({ groups = list(string) }), { groups = [] })<br/>  }))</pre> | `{}` | no |
| <a name="input_user_groups"></a> [user\_groups](#input\_user\_groups) | Okta groups allowed regular user access to Vault. | `list(string)` | `[]` | no |
| <a name="input_user_oidc_logging"></a> [user\_oidc\_logging](#input\_user\_oidc\_logging) | Log received OIDC tokens and claims from non-admin users when debug-level logging is active. | `bool` | `false` | no |
| <a name="input_vault_addr"></a> [vault\_addr](#input\_vault\_addr) | Vault server URL. | `string` | n/a | yes |
| <a name="input_vault_approle_roles"></a> [vault\_approle\_roles](#input\_vault\_approle\_roles) | Map of additional AppRole roles to configure for authentication in Vault. | <pre>map(<br/>    object({<br/>      token_policies = list(string)<br/>    })<br/>  )</pre> | `{}` | no |
| <a name="input_vault_cluster_public_addr"></a> [vault\_cluster\_public\_addr](#input\_vault\_cluster\_public\_addr) | Vault server URL for the cluster public Istio ingress. | `string` | `"https://vault.pre-gitlab-gke.us-east1.gitlab-pre.gke.gitlab.net"` | no |
| <a name="input_vault_kv_mounts"></a> [vault\_kv\_mounts](#input\_vault\_kv\_mounts) | Map of additional key/value mountpoints to enable in Vault. | <pre>map(<br/>    object({<br/>      description = string<br/>      version     = string<br/>    })<br/>  )</pre> | `{}` | no |
| <a name="input_vault_policies"></a> [vault\_policies](#input\_vault\_policies) | Map of additional policies to create in Vault. | `map(any)` | `{}` | no |
| <a name="input_vault_public_addr"></a> [vault\_public\_addr](#input\_vault\_public\_addr) | Vault server URL for the public ingress. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
